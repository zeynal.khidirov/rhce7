# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup program
JAVA_HOME=/tomcat/java_version/jdk-11.0.1
PATH=$PATH:$HOME/.local/bin:$HOME/bin:$JAVA_HOME/bin
export JAVA_HOME=/tomcat/java_version/jdk-19.0.1
export PATH=$JAVA_HOME/bin:$PATH
export PATH
